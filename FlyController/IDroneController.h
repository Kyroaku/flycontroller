#pragma once

/// <summary>
/// Drone behavour controller.\n
/// Implementation of this interface sends commands to drone, to control its
/// orientation, gaz and angular velocity.
/// </summary>
class IDroneController {
public:
	virtual void SetOrientation(float pitch, float yaw, float roll) = 0;
	virtual void SetGaz(float gaz) = 0;
	virtual void SetAngularVelocity(float pitch, float yaw, float roll) = 0;
};