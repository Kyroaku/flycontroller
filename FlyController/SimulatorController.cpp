#include "SimulatorController.h"

#include <Utility/NetworkEvent.h>
#include <Utility/Logger.h>

SimulatorController::SimulatorController()
{
	mSocket = make_unique<Socket>();
	mSocket->Create(Socket::eTcpProtocol);
	Connect();
}

void SimulatorController::SetOrientation(float pitch, float yaw, float roll)
{
	NetworkEvent e;
	e.mVector = Vector3{ pitch, yaw, roll };
	e.mType = NetworkEventType::eOrientation;
	Send(e);
}

void SimulatorController::SetGaz(float gaz)
{
	NetworkEvent e;
	e.mGaz = gaz;
	e.mType = NetworkEventType::eGaz;
	Send(e);
}

void SimulatorController::SetAngularVelocity(float pitch, float yaw, float roll)
{
	NetworkEvent e;
	e.mVector = Vector3{ pitch, yaw, roll };
	e.mType = NetworkEventType::eAngularVelocity;
	Send(e);
}

void SimulatorController::Connect()
{
	Logger::Info("Connecting controller...");
	while (!mSocket->IsConnected()) {
		if (mSocket->Connect(NetworkAddress(IP, PORT)))
			Logger::Success("Controller connected to " + IP + ":" + to_string(PORT));
		else
			Logger::Error("Couldn't connect controller to " + IP + ":" + to_string(PORT));
		Sleep(2000);
	}
}

void SimulatorController::Send(NetworkEvent &e)
{
	if(!mSocket->Send(&e, sizeof(e))) {
		Logger::Warning("Controller disconnected");
		mSocket->Close();
		mSocket->Create(Socket::eTcpProtocol);
		Connect();
	}
}
