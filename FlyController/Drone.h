#pragma once

#include <memory>

#include "IVideoReceiver.h"
#include "IDroneController.h"

using namespace std;

/// <summary>
/// Abstract drone controller class. After conneting implementations of video and move controllers, this class
/// is used as a bridge to controlling drone.
/// </summary>
class Drone {
private:
	/// <summary>
	/// Video receive controller. Connecting different implementation of this controller
	/// allows getting video from different sources (real drone, simulator, etc).
	/// </summary>
	shared_ptr<IVideoReceiver> mVideoReceiver;
	/// <summary>
	/// Drone behaviour controller. Connecting different implementation of this controller
	/// allows controlling different drones (real drone, simulated drone, etc).
	/// </summary>
	shared_ptr<IDroneController> mController;

public:
	/// <summary>
	/// Sets video receive controller. Implementation of IVideoReceiver interface describes 
	/// how to receive video from drone. To receive stream from simulator, use ImageReceiver.
	/// </summary>
	/// <param name="receiver">The controller.</param>
	void SetVideoReceiver(shared_ptr<IVideoReceiver> receiver);
	/// <summary>
	/// Sets drone behaviour controller. Implementation of IDroneController interface describes
	/// how to send commands, to make the drone move or rotate.
	/// </summary>
	/// <param name="controller">The controller.</param>
	void SetController(shared_ptr<IDroneController> controller);

	/// <summary>
	/// Returns image from video stream that is received by video controller.
	/// </summary>
	/// <returns>Image from drone's camera.</returns>
	Image GetImage();

	/// <summary>
	/// Sets orientation of the drone.\n
	/// Detailed behaviour depends on implementation of drone behaviour controller.
	/// </summary>
	/// <param name="pitch">The pitch angle.</param>
	/// <param name="yaw">The yaw angle.</param>
	/// <param name="roll">The roll angle.</param>
	void SetOrientation(float pitch, float yaw, float roll);
	/// <summary>
	/// Sets the drone's gaz.\n
	///Detailed behaviour depends on implementation of drone behaviour controller.
	/// </summary>
	/// <param name="gaz">The gaz.</param>
	void SetGaz(float gaz);
	/// <summary>
	/// Sets the angular velocity of the drone.\n
	/// Detailed behaviour depends on implementation of drone behvaiour controller.
	/// </summary>
	/// <param name="pitch">The pitch angle.</param>
	/// <param name="yaw">The yaw angle.</param>
	/// <param name="roll">The roll angle.</param>
	void SetAngularVelocity(float pitch, float yaw, float roll);
};