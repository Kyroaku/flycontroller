#pragma once

#include <ProcessingEngine/Image.h>

/// <summary>
/// Drone video controller.\n
/// Implementation of this interface receives image from drone's camera.
/// </summary>
class IVideoReceiver {
public:
	virtual Image GetImage() = 0;
};