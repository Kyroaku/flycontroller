#pragma once

#include <iostream>
#include <chrono>

#include <Utility/Network/NetworkManager.h>
#include <Utility/Logger.h>
#include <Utility/Serialization/ISerializable.h>

#include <ProcessingEngine/Image.h>
#include <ProcessingEngine/ArucoDetector.h>
#include <ProcessingEngine/ColorDetector.h>

#include "ImageReceiver.h"
#include "SimulatorController.h"
#include "Drone.h"
#include "FollowRegulator.h"

#ifdef _DEBUG
#pragma comment(lib, "opencv/lib/x64/Debug/opencv_world400d")
#else
#pragma comment(lib, "opencv/lib/x64/Release/opencv_world400")
#endif

#define MAIN_WINDOW_NAME		"Fly Controller"

using namespace std;

/// <summary>
/// Fly controller's main class that is entry point of the application.
/// </summary>
class FlyController {
public:
	/// <summary>
	/// This flag set to true means that application is running. Setting it false will cause that application
	/// will leave the loop.
	/// </summary>
	bool mIsRunning = true;

	/// <summary>
	/// Aruco marker detector used to retrieve markers from image.
	/// </summary>
	unique_ptr<IObjectDetector> mArucoDetector;
	/// <summary>
	/// Color detector used to retrieve color based object from image.
	/// </summary>
	unique_ptr<IObjectDetector> mColorDetector;

	/// <summary>
	/// Drone object that is brigde to controlling the real (or simulated) drone.
	/// </summary>
	Drone mDrone;

	/// <summary>
	/// Regulator object that calculates values to reach good drone's move characteristic.
	/// </summary>
	FollowRegulator mRegulator;

	/// <summary>
	/// Marker ID that is actual target. Drone is searching for marker with this ID.
	/// </summary>
	uint32_t mTargetId = 0;

public:
	/// <summary>
	/// Initializes application and enters the application loop.
	/// </summary>
	void Run();

private:
	/// <summary>
	/// Initializes fly controller.
	/// </summary>
	void Init();
	/// <summary>
	/// Processes the image and controls drone to reach found marker.
	/// </summary>
	/// <param name="img">Image from drone's camera.</param>
	void ProcessImage(Image &img);

	/// <summary>
	/// Calculates pseudo 3D position of marker. Position is relative to drone (drone is a [0,0,0] point).
	/// It is used as a input parameters to the regulator.
	/// </summary>
	/// <param name="marker">The marker.</param>
	/// <returns></returns>
	Point3f Get3DPosition(DetectedMarker &marker);
	/// <summary>
	/// Approximates orientation of the marker and calculates drone's yaw velocity.
	/// </summary>
	/// <param name="marker">The marker.</param>
	/// <returns></returns>
	float GetYawAngle(DetectedMarker &marker);

	bool IsPointInPolygon(Point2f p, vector<Point2f> poly);
};