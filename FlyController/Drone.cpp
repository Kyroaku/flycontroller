#include "Drone.h"

#define CLAMP(x, low, high) (min(max(x,low), high))

void Drone::SetVideoReceiver(shared_ptr<IVideoReceiver> receiver)
{
	mVideoReceiver = receiver;
}

void Drone::SetController(shared_ptr<IDroneController> controller)
{
	mController = controller;
}

Image Drone::GetImage()
{
	return mVideoReceiver->GetImage();
}

void Drone::SetOrientation(float pitch, float yaw, float roll)
{
	mController->SetOrientation(
		CLAMP(pitch, -1.0f, 1.0f),
		CLAMP(yaw, -1.0f, 1.0f),
		CLAMP(roll, -1.0f, 1.0f)
	);
}

void Drone::SetGaz(float gaz)
{
	mController->SetGaz(CLAMP(gaz, -1.0f, 1.0f));
}

void Drone::SetAngularVelocity(float pitch, float yaw, float roll)
{
	mController->SetAngularVelocity(pitch, yaw, roll);
}
