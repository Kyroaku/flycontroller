#pragma once

#include <chrono>

#include <opencv/opencv2/core/types.hpp>

/// <summary>
/// Implementation of regulator that calculates force in each of 3 directions that
/// guarantee smooth and fast reaching target.
/// </summary>
class FollowRegulator {
private:
	/// <summary>
	/// Last calculated value of power which should be used to move forward (or backward).
	/// </summary>
	float mForwardPower;
	/// <summary>
	/// Last calculated value of power which should be used to move right (or left).
	/// </summary>
	float mVerticalPower;
	/// <summary>
	/// Last calculated value of power which should be used to move up (or down).
	/// </summary>
	float mHorizontalPower;

	/// <summary>
	/// Previous position of drone (relative to target).
	/// </summary>
	cv::Point3f mPrevPosition;
	/// <summary>
	/// Previous velocity of drone (relative to target).
	/// </summary>
	cv::Point3f mPrevVelocity;
	/// <summary>
	/// Time point in which last drone's position has been calculated. Used to
	/// calculate velocity.
	/// </summary>
	std::chrono::system_clock::time_point mLastPosTime;

	/// <summary>
	/// Flag that specifies if application found target on the last image.
	/// </summary>
	bool mLostMarker = true;

public:
	/// <summary>
	/// Updates force values depending on the 'target' parameter.
	/// </summary>
	/// <param name="target">Position of the target, relative to the drone (drone position is
	/// [0,0,0] point).</param>
	void Follow(cv::Point3f target, float yaw);

	/// <summary>
	/// Gets the forward power.
	/// </summary>
	/// <returns></returns>
	float GetForwardPower();
	/// <summary>
	/// Gets the vertical power.
	/// </summary>
	/// <returns></returns>
	float GetVerticalPower();
	/// <summary>
	/// Gets the horizontal power.
	/// </summary>
	/// <returns></returns>
	float GetHorizontalPower();

	/// <summary>
	/// Determines whether target position and drone's velocity are small. If so, target has been reached.
	/// </summary>
	/// <returns>
	///   <c>true</c> if target is reached otherwise, <c>false</c>.
	/// </returns>
	bool IsTargetReached();

	/// <summary>
	/// Resets the regulator.\n
	/// After calling this method, regulator will skip informations of previous position and velocity.
	/// </summary>
	void Reset();
};