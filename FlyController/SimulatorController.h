#pragma once

#include <memory>

#include "IDroneController.h"

#include <Utility/NetworkEvent.h>
#include <Utility/Network/NetworkManager.h>

using namespace std;

/// <summary>
/// Implementation of drone behaviour controller that sends commands to simulator,
/// to control the drone.
/// </summary>
/// <seealso cref="IDroneController" />
class SimulatorController : public IDroneController {
private:
	/// <summary>
	/// The ip address of controller server.
	/// </summary>
	const string IP = "127.0.0.1";
	/// <summary>
	/// The port of controller server
	/// </summary>
	const uint16_t PORT = 5556;

	/// <summary>
	/// Controller socket.
	/// </summary>
	unique_ptr<Socket> mSocket;

public:
	/// <summary>
	/// Initializes controller.
	/// </summary>
	SimulatorController();

	/// <summary>
	/// Sets the orientation of drone. Values should be in range from -1 to 1, where -1 is -180 degrees and 1 is 180 degrees.
	/// </summary>
	/// <param name="pitch">The pitch angle.</param>
	/// <param name="yaw">The yaw angle.</param>
	/// <param name="roll">The roll angle.</param>
	void SetOrientation(float pitch, float yaw, float roll);
	/// <summary>
	/// Sets drone's gaz. Value should be in range from -1 to 1, where:\n
	/// -1 -> moving downward with maximum power\n
	/// 0 -> hovering\n
	/// 1 -> moving upward with maximum power
	/// </summary>
	/// <param name="gaz">The gaz value.</param>
	void SetGaz(float gaz);
	/// <summary>
	/// Sets the drone's angular velocity. Values should be in range from -1 to 1, where -1 and 1 means maximum angular speed.
	/// </summary>
	/// <param name="pitch">The pitch velocity.</param>
	/// <param name="yaw">The yaw velocity.</param>
	/// <param name="roll">The roll velocity.</param>
	void SetAngularVelocity(float pitch, float yaw, float roll);

private:
	/// <summary>
	/// Connects to controller socket.
	/// </summary>
	void Connect();
	/// <summary>
	/// Sends command to the drone.
	/// </summary>
	/// <param name="e">The e.</param>
	void Send(NetworkEvent &e);
};