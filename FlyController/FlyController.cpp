#include "FlyController.h"

void FlyController::Run()
{
	Logger::Success("FlyController started");

	/* Init application */
	Init();

	Image img;

	/* Application loop */
	while (mIsRunning) {

		/* Receive image */
		img = mDrone.GetImage();

		/* Process image */
		if (!img.GetMat().empty()) {
			ProcessImage(img);
			img.Show(MAIN_WINDOW_NAME);
		}

		/* Handle keyboard */
		int key = waitKey(1);
		switch (key) {
		case 27:
			mIsRunning = false;
			break;

		case 'w':
			break;

		case 's':
			break;

		case 'a':
			break;

		case 'd':
			break;
		}
	}
}


void FlyController::Init()
{
	/* Initialize sockets */
	if (!Network.Init())
		Logger::Error("Network init error");

	/* Set drone controllers */
	mDrone.SetVideoReceiver(make_shared<ImageReceiver>());
	mDrone.SetController(make_shared<SimulatorController>());

	mColorDetector = make_unique<ColorDetector>();
	mArucoDetector = make_unique<ArucoDetector>();
}

void FlyController::ProcessImage(Image & img)
{
	/* Detect markers */
	auto markers1 = mColorDetector->DetectMarkers(img.GetMat(), true);
	auto markers2 = mArucoDetector->DetectMarkers(img.GetMat(), true);

	bool targetMarkerAlert = false;

	DetectedMarker *targetMarker = nullptr;

	/* Find marker with specified ID */
	for (auto &marker : markers2) {
		if (marker.mMarkerId == mTargetId) {
			targetMarker = &marker;
		}
	}

	/* Marker not found - stabilize drone and return */
	if (targetMarker == nullptr) {
		mDrone.SetOrientation(0.0f, 0.0f, 0.0f);
		mDrone.SetGaz(0.0f);
		mDrone.SetAngularVelocity(0.0f, -0.5f, 0.0f);
		mRegulator.Reset();
		return;
	}

	for (auto &marker : markers1) {
		Point3f p = Get3DPosition(marker);
		Point2f pos = Point2f(p.x, p.y);
		cv::circle(img.GetMat(), pos, 5, cv::Scalar(0, 255, 0));
		if (IsPointInPolygon(pos, targetMarker->mVertices)) {
			cv::Size textSize = cv::getTextSize("ALERT", 5, 5, 4, 0);
			cv::putText(img.GetMat(), "ALERT", cv::Point2f(pos.x - textSize.width/2, pos.y + textSize.height/2), 5, 5, cv::Scalar(0, 255, 255), 4);
			targetMarkerAlert = true;
		}
	}

	/* Get marker position */
	Point3f target = Get3DPosition(*targetMarker);
	/* Get yaw velocity based on marker's orientation */
	float yaw = GetYawAngle(*targetMarker) * 0.7f;

	/* Normalize values */
	target.x /= img.GetMat().size().width;
	target.y /= img.GetMat().size().height;
	target.z /= img.GetMat().size().height;
	target -= Point3f(0.5f, 0.5f, 0.15f);

	/* Calculate regulated values */
	mRegulator.Follow(target, yaw);

	/* Get regulator output */
	float forwardPower = mRegulator.GetForwardPower();
	float horizontalPower = mRegulator.GetHorizontalPower();
	float verticalPower = mRegulator.GetVerticalPower();

	/* Next target */
	if (mRegulator.IsTargetReached()
		&& yaw < 0.001f
		&& targetMarkerAlert == false) {
		mTargetId = (mTargetId + 1) % 4;
		mRegulator.Reset();
	}

	/* Control drone */
	mDrone.SetOrientation(forwardPower, 0.0f, horizontalPower);
	mDrone.SetGaz(verticalPower);
	mDrone.SetAngularVelocity(0.0f, yaw, 0.0f);
}

Point3f FlyController::Get3DPosition(DetectedMarker & marker)
{
	Point3f pos = Point3f(0.0f);
	size_t numVerts = marker.mVertices.size();
	/* Average value of all points is a center of marker (x,y).
	'z' component is the longest side of the marker */
	for (size_t i = 0; i < numVerts; i++) {
		auto &p1 = marker.mVertices.at(i);
		auto &p2 = marker.mVertices.at((i + 1) % numVerts);
		pos.x += p1.x;
		pos.y += p1.y;
		pos.z = max(pos.z, sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p2.y - p2.y)));
	}
	pos.x /= (float)marker.mVertices.size();
	pos.y /= (float)marker.mVertices.size();

	return pos;
}

float FlyController::GetYawAngle(DetectedMarker & marker)
{
	/* We assume that vertices of marker's contour starts in the left top corner
	and goes clock wise. */
	auto &p1 = marker.mVertices.at(0);
	auto &p2 = marker.mVertices.at(1);
	auto &p3 = marker.mVertices.at(2);
	auto &p4 = marker.mVertices.at(3);
	/* First horizontal side */
	//float hl = sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
	float hl = abs(p1.x - p2.x);
	/* First vertical side */
	float vl = sqrt((p3.x - p2.x)*(p3.x - p2.x) + (p3.y - p2.y)*(p3.y - p2.y));
	/* Second vertical side */
	float vl2 = sqrt((p1.x - p4.x)*(p1.x - p4.x) + (p1.y - p4.y)*(p1.y - p4.y));
	/* Difference between length of horizontal and vertical sides means that marker is rotated */
	float yaw = acos(min(hl, vl) / vl);
	/* Direction in which marker is rotated is specified by comparing vertical sides of marker */
	if (vl > vl2)
		yaw = -yaw;
	return yaw;
}

bool FlyController::IsPointInPolygon(Point2f p, vector<Point2f> poly) {
	bool side = false;
	for (size_t i = 0; i < poly.size(); i++) {
		size_t i1 = (i + 1) % poly.size();
		Point2f &p1 = poly.at(i);
		Point2f &p2 = poly.at(i1);
		float d = (p.x - p1.x) * (p2.y - p1.y) - (p.y - p1.y) * (p2.x - p1.x);
		if (side != (d < 0.0f) && i > 0)
			return false;
		side = (d < 0.0f);
	}

	return true;
}