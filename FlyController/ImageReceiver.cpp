#include "ImageReceiver.h"

#include <Utility/Serialization/SerializedData.h>

#include <Utility/Logger.h>

ImageReceiver::ImageReceiver()
{
	mSocket = make_shared<Socket>();
	mSocket->Create(Socket::eTcpProtocol);
	Connect();
}

Image ImageReceiver::GetImage()
{
	Image img;

	/* Receive data size */
	uint32_t size = 0;
	mSocket->Receive(&size, sizeof(size), sizeof(size));

	SerializedData data(size);

	if (mSocket->Receive(&data.GetByteArray()->data()[0], size, size) > 0)
		img.Deserialize(data);
	else {
		Logger::Warning("Video disconnected");
		mSocket->Close();
		mSocket->Create(Socket::eTcpProtocol);
		Connect();
	}

	return img;
}

void ImageReceiver::Connect()
{
	Logger::Info("Connecting video...");
	while (!mSocket->IsConnected()) {
		if (mSocket->Connect(NetworkAddress(IP, PORT)))
			Logger::Success("Video connected to " + IP + ":" + to_string(PORT));
		else
			Logger::Error("Couldn't connect video to " + IP + ":" + to_string(PORT));
		Sleep(2000);
	}
}
