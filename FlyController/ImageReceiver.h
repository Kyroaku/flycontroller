#pragma once

#include <Utility/Network/Socket.h>

#include "IVideoReceiver.h"

/// <summary>
/// Implementation of video controller that receives video from simulator.
/// </summary>
/// <seealso cref="IVideoReceiver" />
class ImageReceiver : public IVideoReceiver {
private:
	/// <summary>
	/// The ip address of video server.
	/// </summary>
	const string IP = "127.0.0.1";
	/// <summary>
	/// The port of the video server.
	/// </summary>
	const uint16_t PORT = 5555;

	/// <summary>
	/// Video socket.
	/// </summary>
	shared_ptr<Socket> mSocket;

public:
	/// <summary>
	/// Initializes video receiver.
	/// </summary>
	ImageReceiver();
	/// <summary>
	/// Receives image from drone simulator.
	/// </summary>
	/// <returns>Received image from drone's camera</returns>
	Image GetImage();

private:
	/// <summary>
	/// Connects to the video socket.
	/// </summary>
	void Connect();
};