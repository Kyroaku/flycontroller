#include "FollowRegulator.h"

void FollowRegulator::Follow(cv::Point3f target, float yaw)
{
	/* Simple kalman filter for target position */
	float k = 1.0f;
	if (!mLostMarker)
		target = target * k + mPrevPosition * (1.0f - k);

	/* Calculate derivatives */
	float t = std::chrono::duration<float>(std::chrono::system_clock::now() - mLastPosTime).count();
	mLastPosTime = std::chrono::system_clock::now();

	cv::Point3f dp = cv::Point3f(
		(target.x - mPrevPosition.x) / t,
		(target.y - mPrevPosition.y) / t,
		(target.z - mPrevPosition.z) / t
	);

	/* If marker was not found in previous frame, reset derivatives */
	if (mLostMarker) {
		dp = cv::Point3f(0.0f);
		mLostMarker = false;
	}

	/* Actual regulator equations */
	mForwardPower = -target.z / 15.0f - dp.z * 0.4f;
	mHorizontalPower = target.x / 15.0f + dp.x * 0.15f;
	mVerticalPower = -(target.y / 1.0f + dp.y * 1.7f + mForwardPower * 3.0f);

	/* Yaw angle impact */
	float yawFactorForward = 1.0f / (1 + abs(yaw) * 0.9f);
	float yawFactorHorizontal = 1.0f / (1 + abs(yaw) * 2.0f);
	mForwardPower *= yawFactorForward;
	mHorizontalPower = (mHorizontalPower - yaw * 0.03f) * yawFactorHorizontal;

	/* Update previous values */
	mPrevPosition = target;
	mPrevVelocity = dp;
}

float FollowRegulator::GetForwardPower()
{
	return mForwardPower;
}

float FollowRegulator::GetVerticalPower()
{
	return mVerticalPower;
}

float FollowRegulator::GetHorizontalPower()
{
	return mHorizontalPower;
}


bool FollowRegulator::IsTargetReached()
{
	return (
		norm(mPrevPosition) < 0.2f &&
		norm(mPrevVelocity) < 0.005f &&
		abs(mPrevPosition.z) < 0.01f
		);
}

void FollowRegulator::Reset()
{
	mLostMarker = true;
	mPrevPosition = cv::Point3f(0.0f);
	mPrevVelocity = cv::Point3f(0.0f);
}
