#pragma once

#include <string>

using std::string;

class NetworkAddress {
public:
	string mIp;
	uint16_t mPort;

	NetworkAddress(string ip = "0.0.0.0", uint16_t port = 0);
};

class BluetoothAddress {
public:
	string mUuid;
	uint64_t mDeviceAddress;

	BluetoothAddress(string uuid, uint64_t deviceAddr);
};