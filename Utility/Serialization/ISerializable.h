#pragma once

#include "SerializedData.h"

class ISerializable {
public:
	virtual ~ISerializable() = default;
	virtual SerializedData Serialize() = 0;
	virtual void Deserialize(SerializedData &data) = 0;
	//virtual void Deserialize(SerializedData data) = delete;
};