#include "SerializedData.h"

#include <fstream>

using std::fstream;

SerializedData::SerializedData() {
	mData = make_shared<vector<uint8_t>>();
}

SerializedData::SerializedData(size_t size)
{
	mData = make_shared<vector<uint8_t>>(size);
}

SerializedData::SerializedData(uint8_t *data, size_t dataLen) {
	mData = make_shared<vector<uint8_t>>(data, &data[dataLen]);
}

SerializedData::SerializedData(string filename)
	: SerializedData()
{
	LoadFromFile(filename);
}

void SerializedData::Allocate(size_t size)
{
	mData->resize(size);
}

void SerializedData::AddData(uint8_t * data, size_t dataLen)
{
	mData->insert(mData->end(), data, data + dataLen);
	mWritePointer += dataLen;
}

bool SerializedData::LoadFromFile(string filename)
{
	fstream file;
	file.open(filename, fstream::in); 
	if (!file.is_open())
		return false;

	char buffer[1024];
	while (!file.eof()) {
		file.read(buffer, 1024);
		this->AddData((uint8_t*)buffer, (size_t)file.gcount());
	}
	return true;
}

bool SerializedData::SaveToFile(string filename)
{
	fstream file;
	file.open(filename, fstream::out);
	if (!file.is_open())
		return false;

	file.write((const char*)&GetByteArray()->front(), GetSize());
	file.close();
	return true;
}

shared_ptr<vector<uint8_t>> SerializedData::GetByteArray() {
	return mData;
}

size_t SerializedData::GetSize() {
	return (mWritePointer - mReadPointer);
}