#pragma once

/// <summary>
/// Types of events (commands) sent to simulated drone.
/// </summary>
enum NetworkEventType : uint8_t {
	/// <summary>
	/// Set drone's velocity
	/// </summary>
	eVelocity,
	/// <summary>
	/// Set drone's orientation
	/// </summary>
	eOrientation,
	/// <summary>
	/// Set drone's gaz
	/// </summary>
	eGaz,
	/// <summary>
	/// Set drone's angular velocity
	/// </summary>
	eAngularVelocity
};

/// <summary>
/// Simple vector type used in every command that needs vector.
/// </summary>
class Vector3 {
public:
	float x, y, z;
};

/// <summary>
/// Command that is sent to control the simulated dorne.
/// </summary>
struct NetworkEvent {
	/// <summary>
	/// Type of the command\n.
	/// </summary>
	NetworkEventType mType;

	union {
		/// <summary>
		/// Command data (only for commands that uses vector)
		/// </summary>
		Vector3 mVector;
		/// <summary>
		/// Command data (only for commands that uses gaz)
		/// </summary>
		float mGaz;
	};
};