#version 330 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vVS[];

out GS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vGS;  

void main()
{
	vGS.vPosition = vVS[0].vPosition;
	vGS.vColor = vVS[0].vColor;
	vGS.vNormal = vVS[0].vNormal;
	vGS.vTexCoord = vVS[0].vTexCoord;
	vGS.vTangentRotation = vVS[0].vTangentRotation;
	gl_Position = gl_in[0].gl_Position; 
    EmitVertex();

	vGS.vPosition = vVS[1].vPosition;
	vGS.vColor = vVS[1].vColor;
	vGS.vNormal = vVS[1].vNormal;
	vGS.vTexCoord = vVS[1].vTexCoord;
	vGS.vTangentRotation = vVS[1].vTangentRotation;
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();
	
	vGS.vPosition = vVS[2].vPosition;
	vGS.vColor = vVS[2].vColor;
	vGS.vNormal = vVS[2].vNormal;
	vGS.vTexCoord = vVS[2].vTexCoord;
	vGS.vTangentRotation = vVS[2].vTangentRotation;
	gl_Position = gl_in[2].gl_Position;
    EmitVertex();
    
    EndPrimitive();
}