#include "ArucoDetector.h"

ArucoDetector::ArucoDetector(aruco::PREDEFINED_DICTIONARY_NAME dictionary /* = aruco::PREDEFINED_DICTIONARY_NAME::DICT_6X6_250 */)
{
	CreateDictionary(dictionary);
}

void ArucoDetector::CreateDictionary(aruco::PREDEFINED_DICTIONARY_NAME dictionary)
{
	mDictionary = aruco::getPredefinedDictionary(dictionary);
}

vector<DetectedMarker> ArucoDetector::DetectMarkers(Mat &img, bool drawMarkers /* = false */)
{
	vector<DetectedMarker> markers;
	std::vector<int>ids;
	std::vector<std::vector<Point2f>>corners;
	aruco::detectMarkers(img, mDictionary, corners, ids);
	for (size_t i = 0; i < ids.size(); i++) {
		markers.push_back(DetectedMarker(ids.at(i), corners.at(i)));
	}
	if (drawMarkers)
		aruco::drawDetectedMarkers(img, corners, ids);

	return markers;
}

Image ArucoDetector::GenerateMarkerImage(int id, int size)
{
	Mat img;
	aruco::drawMarker(mDictionary, id, size, img);
	return Image(img);
}