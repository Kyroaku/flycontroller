#pragma once

#include <vector>
#include <opencv.hpp>

#include "DetectedMarker.h"

class IObjectDetector {
public:
	virtual std::vector<DetectedMarker> DetectMarkers(cv::Mat &img, bool drawMarkers = false) = 0;
};