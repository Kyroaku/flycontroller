#include "Image.h"

Image::Image(Mat img)
{
	mData = img;
}

void Image::Show(string windowName)
{
	imshow(windowName, mData);
}

Mat &Image::GetMat()
{
	return mData;
}

SerializedData Image::Serialize()
{
	uint32_t width, height;
	width = mData.cols;
	height = mData.rows;
	Mat img = mData.reshape(0, 1);
	SerializedData data;
	data.Push(width);
	data.Push(height);
	data.AddData(img.data, img.total() * img.elemSize());
	return data;
}

void Image::Deserialize(SerializedData & data)
{
	uint32_t width, height;
	data.Pop(width);
	data.Pop(height);
	if (width != mData.cols || height != mData.rows)
		mData = Mat::zeros(width*height, 1, CV_8UC3);
	data.Pop(mData.data, mData.total() * mData.elemSize());
	mData = mData.reshape(0, height);
	flip(mData, mData, 0);
}