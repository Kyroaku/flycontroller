#pragma once

#include <ProEngine3D/ProEngine3D.h>

using namespace glm;

/// <summary>
/// Drone object on the scene.
/// </summary>
class DroneObject {
private:
	vec3 mPosition;
	vec3 mRotation;
	vec3 mVelocity;
	vec3 mAngularVelocity;
	vec3 mAcceleration;
	vec3 mAngularAcceleration;

	vec3 mDestinationRotation;
	float mThrust;

	unique_ptr<Model> mModel;
	Camera mCamera;
	uvec2 mCameraResolution;

public:
	/// <summary>
	/// Initializes a new instance of the <see cref="DroneObject"/> class.
	/// </summary>
	DroneObject();

	/// <summary>
	/// Updates drone object's state.
	/// </summary>
	/// <param name="dt">Duration of the frame. Used to calculate time based variables, like velocity.</param>
	void Update(float dt);
	/// <summary>
	/// Renders the drone object with use of specified shader program.
	/// </summary>
	/// <param name="shader">The shader that will be used to render object.</param>
	void Render(Shader *shader);

	/// <summary>
	/// Gets the position of the object.
	/// </summary>
	/// <returns></returns>
	vec3 GetPosition();
	/// <summary>
	/// Gets the rotation of the object.
	/// </summary>
	/// <returns></returns>
	vec3 GetRotation();
	/// <summary>
	/// Gets the velocity of the object.
	/// </summary>
	/// <returns></returns>
	vec3 GetVelocity();

	/// <summary>
	/// Gets the camera view matrix in world space.
	/// </summary>
	/// <returns></returns>
	mat4 GetCameraViewMatrix();
	/// <summary>
	/// Calculates and returns object's model matrix.
	/// </summary>
	/// <returns></returns>
	mat4 GetModelMatrix();

	/// <summary>
	/// Gets the resolution of the drone's camera.
	/// </summary>
	/// <returns></returns>
	uvec2 GetCameraResolution();


	/// <summary>
	/// Gets the destination rotation of a drone.
	/// </summary>
	/// <returns></returns>
	vec3 GetDestinationRotation();

	/// <summary>
	/// Sets the position of the object.
	/// </summary>
	/// <param name="pos">New position.</param>
	void SetPosition(vec3 pos);
	/// <summary>
	/// Sets the velocity of the object.
	/// </summary>
	/// <param name="v">New velocity.</param>
	void SetVelocity(vec3 v);
	/// <summary>
	/// Sets the acceleration of the object.
	/// </summary>
	/// <param name="a">New acceleration.</param>
	void SetAcceleration(vec3 a);

	/// <summary>
	/// Sets the rotation of the object.
	/// </summary>
	/// <param name="rot">New rotation.</param>
	void SetRotation(vec3 rot);
	/// <summary>
	/// Sets the angular velocity of the object.
	/// </summary>
	/// <param name="v">New angular velocity.</param>
	void SetAngularVelocity(vec3 v);
	/// <summary>
	/// Sets the yaw velocity of the object.
	/// </summary>
	/// <param name="v">New yaw velocity.</param>
	void SetYawVelocity(float v);
	/// <summary>
	/// Sets the angular acceleration of the object.
	/// </summary>
	/// <param name="a">New angular acceleration.</param>
	void SetAngularAcceleration(vec3 a);

	/// <summary>
	/// Sets the destination rotation of the object. It will start rotating 
	/// until it reaches specified orientation.
	/// </summary>
	/// <param name="rot">Destination rotation of the object.</param>
	void SetDestinationRotation(vec3 rot);

	/// <summary>
	/// Sets the drone's thrust. Thrust is a force directed upward in drone space.
	/// Value of 0 is equivalent to disabled drone's motors. Settings this parameter
	/// equal to gravity force will cause the drone hovering.
	/// </summary>
	/// <param name="thrust">The thrust.</param>
	void SetThrust(float thrust);
	/// <summary>
	/// Sets the drone's gaz. It is analogues to thrust force, but gaz is relative to gravity force.
	/// It means that settings gaz to 0 will cause the drone hovering.
	/// </summary>
	/// <param name="gaz">The gaz.</param>
	void SetGaz(float gaz);

	/// <summary>
	/// Sets the resolution of drone's camera.
	/// </summary>
	/// <param name="resolution">New drone's camera resolution.</param>
	void SetCameraResolution(uvec2 resolution);
};