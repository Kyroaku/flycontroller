#include "DroneObject.h"

#define GRAVITY					(vec3(0.0f, -9.8*50.0, 0.0f))
#define ROTATION_SPEED			(4.0f)
#define THRUST_ACCELERATION		(60.0f)

DroneObject::DroneObject()
	: mPosition(vec3(0.0f))
	, mVelocity(vec3(0.0f))
	, mAcceleration(vec3(0.0f))
	, mRotation(vec3(0.0f))
	, mAngularVelocity(vec3(0.0f))
	, mAngularAcceleration(vec3(0.0f))
	, mDestinationRotation(vec3(0.0f, 0.0f, 0.0f))
	, mThrust(length(GRAVITY))
	, mCameraResolution(uvec2(640, 480))
{
	mModel = std::make_unique<Model>();
	mModel->LoadFromFile("Data/drone.fbx");

	mCamera.SetPosition(vec3(0.0f, 0.0f, 10.0f));
	mCamera.SetLookAt(mCamera.GetPosition() + vec3(0.0f, 0.0f, 1.0f));
}

void DroneObject::Update(float dt)
{
	/* Linear move */
	vec3 a = mAcceleration + mat3(GetModelMatrix()) * vec3(0.0f, mThrust, 0.0f);
	mVelocity += a * dt + GRAVITY * dt;
	mPosition += mVelocity * dt;

	/* Angular move */
	mAngularVelocity += mAngularAcceleration * dt;
	mDestinationRotation.y = mRotation.y;
	vec3 randvec = vec3((float)rand() / RAND_MAX - 0.5f) / 20.0f;
	vec3 av = mAngularVelocity + (mDestinationRotation + randvec - mRotation) * ROTATION_SPEED;
	mRotation += av * dt;
}

void DroneObject::Render(Shader * shader)
{
	mModel->Render(shader, GetModelMatrix());
}



vec3 DroneObject::GetPosition()
{
	return mPosition;
}

vec3 DroneObject::GetRotation()
{
	return mRotation;
}

vec3 DroneObject::GetVelocity()
{
	return mVelocity;
}

mat4 DroneObject::GetCameraViewMatrix()
{
	return mCamera.GetViewMatrix() * inverse(GetModelMatrix());
}

mat4 DroneObject::GetModelMatrix()
{
	mat4 trans = mat4(1.0f);
	trans = translate(trans, mPosition);
	trans = rotate(trans, mRotation.y + pi<float>(), vec3(0.0f, 1.0f, 0.0f));
	trans = rotate(trans, mRotation.x, vec3(1.0f, 0.0f, 0.0f));
	trans = rotate(trans, mRotation.z, vec3(0.0f, 0.0f, 1.0f));
	return trans;
}

uvec2 DroneObject::GetCameraResolution()
{
	return mCameraResolution;
}

vec3 DroneObject::GetDestinationRotation()
{
	return mDestinationRotation;
}

void DroneObject::SetPosition(vec3 pos)
{
	mPosition = pos;
}

void DroneObject::SetRotation(vec3 rot)
{
	mRotation = rot;
}

void DroneObject::SetAngularVelocity(vec3 v)
{
	mAngularVelocity = v;
}

void DroneObject::SetYawVelocity(float v)
{
	mAngularVelocity.y = v;
}

void DroneObject::SetAngularAcceleration(vec3 a)
{
	mAngularAcceleration = a;
}

void DroneObject::SetVelocity(vec3 v)
{
	mVelocity = v;
}

void DroneObject::SetAcceleration(vec3 a)
{
	mAcceleration = a;
}

void DroneObject::SetDestinationRotation(vec3 rot)
{
	mDestinationRotation = rot;
}

void DroneObject::SetThrust(float thrust)
{
	mThrust = thrust;
}

void DroneObject::SetGaz(float gaz)
{
	SetThrust(length(GRAVITY) + gaz * THRUST_ACCELERATION);
}

void DroneObject::SetCameraResolution(uvec2 resolution)
{
	mCameraResolution = resolution;
}
