#pragma once

#include <vector>
#include <opencv.hpp>

using namespace cv;
using std::vector;

/// <summary>
/// Class represents informations about detected marker.
/// </summary>
class DetectedMarker {
public:
	DetectedMarker() = default;
	DetectedMarker(int id, vector<Point2f> vertices)
		: mMarkerId(id)
		, mVertices(vertices)
	{
	}

	/// <summary>
	/// List of vertices that create marker's contour (or bounding box).
	/// </summary>
	vector<Point2f> mVertices;

	/// <summary>
	/// The marker ID.
	/// </summary>
	int mMarkerId = 0;
};