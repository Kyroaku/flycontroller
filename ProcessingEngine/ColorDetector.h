#pragma once

#include "DetectedMarker.h"
#include "IObjectDetector.h"

/// <summary>
/// Implementation of detecting object with specified color
/// </summary>
/// <seealso cref="IObjectDetector" />
class ColorDetector : public IObjectDetector {
private:
	/// <summary>
	/// Object's color hue range
	/// </summary>
	uint8_t mHueRange[2] = { 0, 5 };
	/// <summary>
	/// Object's color saturation range
	/// </summary>
	uint8_t mSaturationRange[2] = { 120, 255 };
	/// <summary>
	/// Object's color value range
	/// </summary>
	uint8_t mValueRange[2] = { 50, 255 };

public:
	/// <summary>
	/// Detects object with specific color.
	/// </summary>
	/// <param name="img">The input image.</param>
	/// <param name="drawMarkers">if set to <c>true</c>, bounding box is drawn on the input image.</param>
	/// <returns>List of bounding boxes of detected objects.</returns>
	vector<DetectedMarker> DetectMarkers(Mat &img, bool drawMarkers = false);

	/// <summary>
	/// Sets the hue range.
	/// </summary>
	/// <param name="min">The minimum.</param>
	/// <param name="max">The maximum.</param>
	void SetHueRange(uint8_t min, uint8_t max);
	/// <summary>
	/// Sets the saturation range.
	/// </summary>
	/// <param name="min">The minimum.</param>
	/// <param name="max">The maximum.</param>
	void SetSaturationRange(uint8_t min, uint8_t max);
	/// <summary>
	/// Sets the value range.
	/// </summary>
	/// <param name="min">The minimum.</param>
	/// <param name="max">The maximum.</param>
	void SetValueRange(uint8_t min, uint8_t max);
};