#pragma once

#include <opencv2/aruco.hpp>

#include "Image.h"
#include "DetectedMarker.h"
#include "IObjectDetector.h"

using namespace cv;

/// <summary>
/// Aruco detector manager that allows to generate and detect aruco markers from choosen dictionary.
/// </summary>
class ArucoDetector : public IObjectDetector {
private:
	Ptr<aruco::Dictionary> mDictionary;

public:
	/// <summary>
	/// Initializes aruco detector with specified dictionary.
	/// </summary>
	/// <param name="dictionary">The dictionary.</param>
	ArucoDetector(aruco::PREDEFINED_DICTIONARY_NAME dictionary = aruco::PREDEFINED_DICTIONARY_NAME::DICT_6X6_250);

	/// <summary>
	/// Creates the dictionary for generating and detecting aruco markers.
	/// </summary>
	/// <param name="dictionary">The dictionary.</param>
	void CreateDictionary(aruco::PREDEFINED_DICTIONARY_NAME dictionary);

	/// <summary>
	/// Detects Aruco markers on the passed image.
	/// </summary>
	/// <param name="img">The image that potentially contains Aruco markers.</param>
	/// <param name="drawMarkers">if 'true', detected markers are marked on the input image.</param>
	/// <returns>
	/// List of detected markers.
	/// </returns>
	vector<DetectedMarker> DetectMarkers(Mat &img, bool drawMarkers = false);

	/// <summary>
	/// Generates the marker image.
	/// </summary>
	/// <param name="id">The marker ID.</param>
	/// <param name="size">The marker size.</param>
	/// <returns>
	/// Aruco marker image with specified ID and size.
	/// </returns>
	Image GenerateMarkerImage(int id, int size);
};