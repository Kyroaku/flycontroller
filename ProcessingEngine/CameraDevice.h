#pragma once

#include "Image.h"

#include <opencv.hpp>
#include <opencv2\aruco.hpp>

using namespace cv;

/// <summary>
/// Class represents camera object that allows to grab frame from choosen physical camera.
/// </summary>
class CameraDevice {
private:
	VideoCapture mVideoCapture;

public:
	/// <summary>
	/// Default constructor.
	/// </summary>
	CameraDevice() = default;
	/// <summary>
	/// Initializes instance and opens camera with specified ID. Maximum possible camera's resolution is set.
	/// </summary>
	/// <param name="cameraId">The camera device ID.</param>
	CameraDevice(int cameraId);

	/// <summary>
	/// Opens the camera with specified ID.
	/// </summary>
	/// <param name="cameraId">The camera device identifier.</param>
	/// <returns></returns>
	bool Open(int cameraId);

	/// <summary>
	/// Gets the frame from actually open physical camera.
	/// </summary>
	/// <returns>Image grabbed from camera.</returns>
	Image GetFrame();
};