#include "CameraDevice.h"

CameraDevice::CameraDevice(int cameraId)
{
	Open(cameraId);
}

bool CameraDevice::Open(int cameraId)
{
	if (mVideoCapture.open(0) == true) {
		mVideoCapture.set(CAP_PROP_FRAME_WIDTH, 9999);
		mVideoCapture.set(CAP_PROP_FRAME_HEIGHT, 9999);
		return true;
	}
	else
		return false;
}

Image CameraDevice::GetFrame()
{
	Mat img;

	if (mVideoCapture.read(img))
		return Image(img);
	else
		return Image();
}
