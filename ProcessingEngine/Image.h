#pragma once

#include <string>
#include <opencv/opencv.hpp>

#include <Utility/Serialization/ISerializable.h>

using namespace std;
using namespace cv;

/// <summary>
/// Class represents serializable image object.
/// </summary>
/// <seealso cref="ISerializable" />
class Image : public ISerializable {
private:
	Mat mData;

public:
	/// <summary>
	/// Default constructor.
	/// </summary>
	Image() = default;

	/// <summary>
	/// Initializes object with specified image data.
	/// </summary>
	/// <param name="img">The image data.</param>
	Image(Mat img);

	/// <summary>
	/// Displays image on new window with specified name.
	/// </summary>
	/// <param name="windowName">Name of the window.</param>
	void Show(string windowName);

	/// <summary>
	/// Returns image data as a OpenCV's Mat object.
	/// </summary>
	Mat &GetMat();

	/// <summary>
	/// Serializes this instance.
	/// </summary>
	/// <returns></returns>
	SerializedData Serialize();

	/// <summary>
	/// Deserializes the specified data.
	/// </summary>
	/// <param name="data">The data.</param>
	void Deserialize(SerializedData &data);
};