#include "ColorDetector.h"

vector<DetectedMarker> ColorDetector::DetectMarkers(Mat &img, bool drawMarkers /* = false */)
{
	vector<DetectedMarker> markers;

	Mat tmp;
	/* Convert image to HSV */
	cvtColor(img, tmp, COLOR_BGR2HSV);
	/* Apply threshold to retrieve specific color */
	inRange(
		tmp,
		Scalar(mHueRange[0], mSaturationRange[0], mValueRange[0]),
		Scalar(mHueRange[1], mSaturationRange[1], mValueRange[1]),
		tmp
	);

	vector<vector<Point>> contours;

	/* Find object contours */
	findContours(tmp, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	//drawContours(img, contours, -1 /*all contours*/, Scalar(0, 255, 0), 1);

	/* Create bounding boxes of detected objects */
	for (auto &c : contours) {
		Rect bb = boundingRect(c);
		DetectedMarker marker;
		marker.mMarkerId = 0;
		marker.mVertices.push_back(Point2f((float)bb.x, (float)bb.y));
		marker.mVertices.push_back(Point2f((float)bb.x + bb.width, (float)bb.y));
		marker.mVertices.push_back(Point2f((float)bb.x + bb.width, (float)bb.y + bb.height));
		marker.mVertices.push_back(Point2f((float)bb.x, (float)bb.y + bb.height));
		markers.push_back(marker);
		/* Draw bounding box on input image */
		if (drawMarkers)
			rectangle(img, bb, Scalar(0, 255, 0));
	}

	return markers;
}

void ColorDetector::SetHueRange(uint8_t min, uint8_t max)
{
	mHueRange[0] = min;
	mHueRange[1] - max;
}

void ColorDetector::SetSaturationRange(uint8_t min, uint8_t max)
{
	mSaturationRange[0] = min;
	mSaturationRange[1] - max;
}

void ColorDetector::SetValueRange(uint8_t min, uint8_t max)
{
	mValueRange[0] = min;
	mValueRange[1] = max;
}
