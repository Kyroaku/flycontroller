#pragma once

#include <vector>

#include "config.h"

#include "Vertex.h"

using std::vector;

class Mesh {
public:
	enum ERenderMode {
		eTriangles = GL_TRIANGLES,
		eQuads = GL_QUADS,
		eLines = GL_LINES,
		eTriangleStrip = GL_TRIANGLE_STRIP,
		eTriangleFan = GL_TRIANGLE_FAN,
		eQuadStrip = GL_QUAD_STRIP,
		eLineStrip = GL_LINE_STRIP,
		ePoints = GL_POINTS
	};

private:
	vector<Vertex> mVertices;
	vector<uint32_t> mIndices;
	uint32_t mMaterialId = 0;
	GLuint mVAO = 0, mVBO = 0, mEBO = 0;
	ERenderMode mRenderMode = eTriangles;

public:
	PRO_API ~Mesh();

	PRO_API vector<Vertex> &GetVertices();
	PRO_API vector<uint32_t> &GetIndices();
	PRO_API void *GetVertexData();
	PRO_API void *GetIndexData();
	PRO_API size_t GetNumVertices();
	PRO_API size_t GetNumIndices();
	PRO_API uint32_t GetMaterialId();
	PRO_API ERenderMode GetRenderMode();

	PRO_API void SetMaterialId(uint32_t id);
	PRO_API void SetRenderMode(ERenderMode mode);

	PRO_API bool HasIndexedVertices();

	PRO_API void Build();
	PRO_API void UpdateBuild();

	PRO_API void Render();

private:
	static const GLuint PositionAttribIndex;
	static const GLuint ColorAttribIndex;
	static const GLuint TexCoordAttribIndex;
	static const GLuint NormalAttribIndex;
	static const GLuint TangentAttribIndex;
};