#pragma once

#include <memory>

#include "config.h"
#include "Mesh.h"

using std::unique_ptr;

class MeshBuilder {
public:
	PRO_API static unique_ptr<Mesh> Plane();
	PRO_API static unique_ptr<Mesh> Cube();
};