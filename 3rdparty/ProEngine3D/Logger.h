#pragma once

#include <iostream>
#include <string>

/* Console colors */
#include <Windows.h>

#include "config.h"

class Logger {
private:
	enum EColor : unsigned char {
		eBlack = 0,
		eDarkBlue = 1,
		eDarkGreen = 2,
		eDarkCyan = 3,
		eDarkRed = 4,
		eDarkMagenta = 5,
		eDarkYellow = 6,
		eGray = 7,
		eDarkGray = 8,
		eBlue = 9,
		eGreen = 10,
		eCyan = 11,
		eRed = 12,
		eMagenta = 13,
		eYellow = 14,
		eWhite = 15
	};

public:
	static void Debug(std::string msg) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eCyan);
		std::cout << "[Debug] ";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
		std::cout << msg << std::endl;
	}
	static void Info(std::string msg) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eWhite);
		std::cout << "[Info] ";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
		std::cout << msg << std::endl;
	}
	static void Error(std::string msg) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eRed);
		std::cout << "[Error] ";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
		std::cout << msg << std::endl;
	}
	static void Warning(std::string msg) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eYellow);
		std::cout << "[Warning] ";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
		std::cout << msg << std::endl;
	}
	static void Success(std::string msg) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGreen);
		std::cout << "[OK] ";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
		std::cout << msg << std::endl;
	}
};