#include "DroneSimulator.h"

#ifdef _DEBUG
#pragma comment(lib, "opencv/lib/x64/Debug/opencv_world400d")
#else
#pragma comment(lib, "opencv/lib/x64/Release/opencv_world400")
#endif

void DroneSimulator::Run()
{
	Logger::Success("DroneSimulator started");

	InitServer();

	mVideoSendFuture = async(launch::async, [this]() {
		while (mIsRunning) {
			/* Accept connection */
			if (mVideoClient == nullptr || !mVideoClient->IsConnected()) {
				AcceptVideoClient();
			}

			/* Receive frame */
			Image img = mSimulator.GetFrame();

			/* Send frame */
			if (!img.GetMat().empty()) {
				SerializedData data = img.Serialize();
				uint32_t size = (uint32_t)data.GetSize();
				mVideoClient->Send(&size, sizeof(size));
				if (!mVideoClient->Send(data.GetByteArray()->data(), (int)data.GetSize())) {
					Logger::Warning("Connection lost");
					AcceptVideoClient();
				}
			}

			std::this_thread::sleep_for(33ms);
		}
	});

	mControlReceiveFuture = async(launch::async, [this]() {
		while (mIsRunning) {
			/* Accept connection */
			if (mControllerClient == nullptr || !mControllerClient->IsConnected()) {
				AcceptControllerClient();
			}

			NetworkEvent ne;
			if (mControllerClient->Receive(&ne, sizeof(ne), sizeof(ne)) > 0)
				mSimulator.AddNetworkEvent(ne);
		}
	});

	mSimulator.Run(uvec2(1280, 768));
}

void DroneSimulator::InitServer()
{
	if (!Network.Init())
		Logger::Error("Network init error");

	if (!mVideoServer.Create(Socket::eTcpProtocol))
		Logger::Error("Video socket create error");

	if (!mControllerServer.Create(Socket::eTcpProtocol))
		Logger::Error("Controller socket create error");

	const string video_ip = "127.0.0.1";
	const uint16_t video_port = 5555;
	if (mVideoServer.OpenServer(video_ip, video_port))
		Logger::Success("Video server open at " + video_ip + ":" + to_string(video_port));
	else
		Logger::Error("Video server open error");

	const string conrtoller_ip = "127.0.0.1";
	const uint16_t controller_port = 5556;
	if (mControllerServer.OpenServer(conrtoller_ip, controller_port))
		Logger::Success("Controller server open at " + conrtoller_ip + ":" + to_string(controller_port));
	else
		Logger::Error("Controller server open error");
}

void DroneSimulator::AcceptVideoClient()
{
	do {
		mVideoClient = mVideoServer.Accept();
		if (mVideoClient == nullptr)
			Logger::Warning("Video client accept error");
		else {
			Logger::Info("Video connected");
		}
	} while (!mVideoClient->IsConnected());
}

void DroneSimulator::AcceptControllerClient()
{
	do {
		mControllerClient = mControllerServer.Accept();
		if (mControllerClient == nullptr)
			Logger::Warning("Conrtoller client accept error");
		else {
			Logger::Info("Conrtoller connected");
		}
	} while (!mControllerClient->IsConnected());
}
