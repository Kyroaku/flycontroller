#include "DroneObject.h"

DroneObject::DroneObject()
	: mPosition(vec3(0.0f))
	, mRotation(vec3(0.0f))
{
	mModel = std::make_unique<Model>();
	mModel->LoadFromFile("Data/drone.fbx");
}

vec3 DroneObject::GetPosition()
{
	return mPosition;
}

vec3 DroneObject::GetRotation()
{
	return mRotation;
}

void DroneObject::SetPosition(vec3 pos)
{
	mPosition = pos;
}

void DroneObject::SetRotation(vec3 rot)
{
	mRotation = rot;
}
