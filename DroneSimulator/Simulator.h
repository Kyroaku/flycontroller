#pragma once

#include <memory>
#include <queue>

#include <3rdparty/ProEngine3D/ProEngine3D.h>
#include <SFML/Window.hpp>

#include <ProcessingEngine/DroneObject.h>
#include <ProcessingEngine/Image.h>

#include <Utility/NetworkEvent.h>

using namespace std;
using namespace glm;

/// <summary>
/// Main class that implements actual application. It is responsible for handling local and network events,
/// updating application's state and rendering scene.
/// </summary>
class Simulator {
private:
	/// <summary>
	/// Rendering modes that are used to render whole scene few times in different ways.
	/// </summary>
	enum ERenderMode {
		/// <summary>
		/// Normal scene rendering.
		/// </summary>
		eRenderScene,
		/// <summary>
		/// Rendering scene from drone's camera view.
		/// </summary>
		eRenderDroneView
	};

private:
	/// <summary>
	/// SFML window handle.
	/// </summary>
	sf::Window mWindow;

	/// <summary>
	/// Framebuffer used to rendering scene from drone's camera view to texture.
	/// </summary>
	unique_ptr<FrameBuffer> mFrameBuffer;
	/// <summary>
	/// Color buffer for </see cref="mFrameBuffer">.
	/// </summary>
	Texture mColorBuffer;

	/// <summary>
	/// Main camera on the scene.
	/// </summary>
	Camera mCamera;
	/// <summary>
	/// Directional light object
	/// </summary>
	Light mDirLight;

	/// <summary>
	/// Main shader object.
	/// </summary>
	unique_ptr<Shader> mShader;
	unique_ptr<Model> mTerrainModel, mMarker0, mMarker1, mMarker2, mMarker3, mRedCube;
	
	/// <summary>
	/// The drone object.
	/// </summary>
	unique_ptr<DroneObject> mDrone;

	shared_ptr<uint8_t[]> mCameraPixelData;

	queue<NetworkEvent> mNetworkEventQueue;
	mutex mNetworkEventMutex;

	bool mRemoteControl = false;

	bool mRunning;

public:
	void Init(uvec2 resolution);

	void HandleInput(sf::Event &event);
	void HandleNetwork(NetworkEvent &event);
	void Update(float dt);
	void Render();

	void Render(ERenderMode mode);

	Image GetFrame();

	void AddNetworkEvent(NetworkEvent &e);
	bool PollNetworkEvent(NetworkEvent &e);

	void Run(uvec2 resolution);

private:
	unique_ptr<Model> LoadMarker(string path);
};