#include <Utility/Logger.h>

#include "Simulator.h"

#pragma comment(lib, "ProEngine3D/lib/x64/Release/ProEngine3D")

#ifdef _WIN64
#	ifdef _DEBUG
#		pragma comment(lib, "SFML/lib/x64/sfml-window-d")
#		pragma comment(lib, "SFML/lib/x64/sfml-system-d")
#	else
#		pragma comment(lib, "SFML/lib/x64/sfml-window")
#		pragma comment(lib, "SFML/lib/x64/sfml-system")
#	endif
#else
#	error("No 32-bit SFML libs")
#endif

void Simulator::Init(uvec2 resolution)
{
	mWindow.create(
		sf::VideoMode(resolution.x, resolution.y),
		"DroneSimulator",
		sf::Style::Close,
		sf::ContextSettings(24, 8, 0)
	);

	ProEngine3D::Init();
	glViewport(0, 0, resolution.x, resolution.y);

	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
	glClearDepth(1.0f);

	mShader = make_unique<Shader>("Shaders/ShadowMapping");

	mTerrainModel = make_unique<Model>();
	mTerrainModel->LoadFromFile("Data/terrain3.fbx");

	mat4 t;
	mMarker0 = LoadMarker("Data/0w.png");
	t = mMarker0->GetRootNode()->GetTransformation();
	t = translate(t, vec3(-40.0f, 50.0f, -40.0f));
	t = scale(t, vec3(7.0f));
	mMarker0->GetRootNode()->SetTransformation(t);

	mMarker1 = LoadMarker("Data/1w.png");
	t = mMarker1->GetRootNode()->GetTransformation();
	t = translate(t, vec3(0.0f, 50.0f, -40.0f));
	t = scale(t, vec3(7.0f));
	mMarker1->GetRootNode()->SetTransformation(t);

	mMarker2 = LoadMarker("Data/2w.png");
	t = mMarker2->GetRootNode()->GetTransformation();
	t = translate(t, vec3(100.0f, 50.0f, 40.0f));
	t = rotate(t, -pi<float>() / 2.0f, vec3(0.0f, 1.0f, 0.0f));
	t = scale(t, vec3(7.0f));
	mMarker2->GetRootNode()->SetTransformation(t);

	mMarker3 = LoadMarker("Data/3w.png");
	t = mMarker3->GetRootNode()->GetTransformation();
	t = translate(t, vec3(0.0f, 50.0f, 80.0f));
	t = rotate(t, -pi<float>(), vec3(0.0f, 1.0f, 0.0f));
	t = scale(t, vec3(7.0f));
	mMarker3->GetRootNode()->SetTransformation(t);

	mRedCube = make_unique<Model>();
	mRedCube->GetMeshes().push_back(MeshBuilder::Cube());
	mRedCube->GetMeshes().at(0)->SetMaterialId(0);
	mRedCube->GetRootNode()->AddMesh(0);
	mRedCube->GetRootNode()->SetTransformation(scale(translate(mat4(1.0f), vec3(50.0f, 50.0f, -40.0f)), vec3(10.0f)));
	mRedCube->GetMaterials().push_back(make_unique<Material>());
	mRedCube->GetMaterials().at(0)->mColorAmbient = vec3(0.0f);
	mRedCube->GetMaterials().at(0)->mColorDiffuse = vec3(1.0f, 0.0f, 0.0f);
	mRedCube->GetMaterials().at(0)->mColorSpecular = vec3(0.0f);
	mRedCube->GetMaterials().at(0)->mShininess = 16.0f;
	mRedCube->GetMaterials().at(0)->mShininessStrength = 1.0f;

	mDrone = std::make_unique<DroneObject>();
	mDrone->SetPosition(vec3(0.0f, 50.0f, 50.0f));
	//mDrone->SetCameraResolution(uvec2(640, 480));
	mDrone->SetCameraResolution(resolution / (uint32_t)1);

	mCamera.SetPosition(vec3(0.0f, 80.0f, 150.0f));
	mCamera.SetLookAt(vec3(40.0f, 0.0f, 0.0f));
	mCamera.SetPerspective(60.0f, (float)resolution.x / resolution.y, 1.0f, 10000.0f);

	uint32_t numPixels = mDrone->GetCameraResolution().x*mDrone->GetCameraResolution().y;
	mCameraPixelData = shared_ptr<uint8_t[]>(new uint8_t[numPixels * (uint32_t)3]);

	mFrameBuffer = make_unique<FrameBuffer>();
	mColorBuffer.Create(Texture::eTexture2D, mDrone->GetCameraResolution());
	mFrameBuffer->AttachColorBuffer(mColorBuffer);
	mFrameBuffer->AttachDepthStencilBuffer(mDrone->GetCameraResolution());

	mDirLight.mType = Light::eDirectional;
	mDirLight.mDirection = normalize(vec3(1.0f, -1.0f, -1.0f));
	mDirLight.mAmbientColor = vec3(0.4f);
	mDirLight.mDiffuseColor = vec3(1.0f);
	mDirLight.mSpecularColor = vec3(0.3f);
}

void Simulator::HandleInput(sf::Event & event)
{
	float moveAngle = 0.0f;
	float rotSpeed = 0.0f;
	float gaz = 0.0f;

	if (event.type == sf::Event::KeyPressed) {
		moveAngle = 0.3f;
		rotSpeed = 2.0f;
		gaz = 1.0f;
	}

	vec3 dtsRotation = mDrone->GetDestinationRotation();

	switch (event.key.code) {
	case sf::Keyboard::Escape:
		mRunning = false;
		break;

	case sf::Keyboard::W:
		dtsRotation.x = moveAngle;
		mDrone->SetDestinationRotation(dtsRotation);
		break;

	case sf::Keyboard::S:
		dtsRotation.x = -moveAngle;
		mDrone->SetDestinationRotation(dtsRotation);
		break;

	case sf::Keyboard::A:
		dtsRotation.z = -moveAngle;
		mDrone->SetDestinationRotation(dtsRotation);
		break;

	case sf::Keyboard::D:
		dtsRotation.z = moveAngle;
		mDrone->SetDestinationRotation(dtsRotation);
		break;

	case sf::Keyboard::R:
		mDrone->SetGaz(gaz);
		break;

	case sf::Keyboard::F:
		mDrone->SetGaz(-gaz);
		break;

	case sf::Keyboard::Q:
		mDrone->SetYawVelocity(rotSpeed);
		break;

	case sf::Keyboard::E:
		mDrone->SetYawVelocity(-rotSpeed);
		break;

	case sf::Keyboard::Space:
		if (event.type == sf::Event::KeyPressed)
			mRemoteControl = !mRemoteControl;
		break;

	case sf::Keyboard::Numpad1:
	case sf::Keyboard::Numpad2:
	case sf::Keyboard::Numpad3:
	case sf::Keyboard::Numpad4:
		if (event.type == sf::Event::KeyPressed) {
			Model *marker = nullptr;
			switch (event.key.code) {
			case sf::Keyboard::Numpad1: marker = mMarker0.get(); break;
			case sf::Keyboard::Numpad2: marker = mMarker1.get(); break;
			case sf::Keyboard::Numpad3: marker = mMarker2.get(); break;
			case sf::Keyboard::Numpad4: marker = mMarker3.get(); break;
			}

			// If marker is not white
			if (marker->GetMaterials().at(0)->mColorDiffuse.g < 0.5f)
				// Set white
				marker->GetMaterials().at(0)->mColorDiffuse = vec3(1.0f, 1.0f, 1.0f);
			// If marker is not red
			else
				// Set red
				marker->GetMaterials().at(0)->mColorDiffuse = vec3(1.0f, 0.0f, 0.0f);
			
			Logger::Debug("Color changed");
		}
		break;
	}

	if (event.type == sf::Event::MouseMoved) {
		static vec2 prevMousePos = vec2(0.0f);
		vec2 dmouse = vec2(event.mouseMove.x, event.mouseMove.y) - prevMousePos;

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			mat4 lookMat = mCamera.GetViewMatrix();
			lookMat = glm::rotate(lookMat, dmouse.x / 120.0f, vec3(0.0f, 1.0f, 0.0f));
			lookMat = glm::rotate(lookMat, dmouse.y / 120.0f, vec3(lookMat[0].x, lookMat[1].x, lookMat[2].x));

			mCamera.SetLookAt(mCamera.GetPosition() - transpose(mat3(lookMat))[2]);
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
			vec3 pos = mCamera.GetPosition();
			vec3 lookDir = mCamera.GetLookAt() - pos;

			vec3 dpos = vec3(-dmouse.x / 2.0f, 0.0f, -dmouse.y / 2.0f) * mat3(mCamera.GetViewMatrix());
			dpos.y = 0;
			pos += dpos;

			mCamera.SetPosition(pos);
			mCamera.SetLookAt(lookDir + pos);
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Middle)) {
			vec3 pos = mCamera.GetPosition();
			vec3 lookDir = mCamera.GetLookAt() - pos;

			pos += vec3(-dmouse.x / 2.0f, dmouse.y / 2.0f, 0.0f) * mat3(mCamera.GetViewMatrix());

			mCamera.SetPosition(pos);
			mCamera.SetLookAt(lookDir + pos);
		}

		prevMousePos = vec2(event.mouseMove.x, event.mouseMove.y);
	}
}

void Simulator::HandleNetwork(NetworkEvent & event)
{
	switch (event.mType) {
	case NetworkEventType::eVelocity:
		mDrone->SetVelocity(vec3(event.mVector.x, event.mVector.y, event.mVector.z));
		break;

	case NetworkEventType::eOrientation:
		mDrone->SetDestinationRotation(vec3(event.mVector.x, event.mVector.y, event.mVector.z) * pi<float>());
		break;

	case NetworkEventType::eAngularVelocity:
		mDrone->SetAngularVelocity(vec3(event.mVector.x, event.mVector.y, event.mVector.z));
		break;

	case NetworkEventType::eGaz:
		mDrone->SetGaz(event.mGaz);
		break;
	}
}

void Simulator::Update(float dt)
{
	mDrone->Update(dt);
}

void Simulator::Render()
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.05f);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	/* Render fullscreen scene */
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, mWindow.getSize().x, mWindow.getSize().y);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render(eRenderScene);

	/* Render drone camera preview */
	glViewport(0, 0, mWindow.getSize().x / 3, mWindow.getSize().y / 3);
	glScissor(0, 0, mWindow.getSize().x / 3, mWindow.getSize().y / 3);
	glEnable(GL_SCISSOR_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_SCISSOR_TEST);
	Render(eRenderDroneView);

	/* Render drone camera preview to texture */
	mFrameBuffer->Bind();
	glViewport(0, 0, mDrone->GetCameraResolution().x, mDrone->GetCameraResolution().y);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render(eRenderDroneView);

	glReadPixels(0, 0, mDrone->GetCameraResolution().x, mDrone->GetCameraResolution().y, GL_BGR, GL_UNSIGNED_BYTE, mCameraPixelData.get());

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	mWindow.display();
}


void Simulator::Render(ERenderMode mode)
{
	/* Prepare OpenGL */
	mat4 modelViewMat = mat4(1.0f);

	if (mode == eRenderScene) {
		modelViewMat = mCamera.GetMatrix();
	}
	else if (mode == eRenderDroneView) {
		modelViewMat = mCamera.GetProjectionMatrix() * mDrone->GetCameraViewMatrix();
	}
	/* Render scene */
	mShader->Use();
	mShader->Uniform1i("uDiffuseTexture", Material::eDiffuseMap);
	mShader->UniformMatrix4fv("uProjectionViewMatrix", false, &modelViewMat[0][0]);
	mShader->Uniform3fv("uCameraPosition", &mCamera.GetPosition()[0]);
	mShader->UniformLight("uDirLight", mDirLight);

	mTerrainModel->Render(mShader.get());
	mDrone->Render(mShader.get());
	mMarker0->Render(mShader.get());
	mMarker1->Render(mShader.get());
	mMarker2->Render(mShader.get());
	mMarker3->Render(mShader.get());
	mRedCube->Render(mShader.get());
}

Image Simulator::GetFrame()
{
	if (mDrone == nullptr || mCameraPixelData == nullptr)
		return Image();

	Mat img = Mat(
		mDrone->GetCameraResolution().y,
		mDrone->GetCameraResolution().x,
		CV_8UC3,
		mCameraPixelData.get()
	);

	return Image(img);
}

void Simulator::AddNetworkEvent(NetworkEvent & e)
{
	lock_guard<mutex> guard(mNetworkEventMutex);
	mNetworkEventQueue.push(e);
}

bool Simulator::PollNetworkEvent(NetworkEvent & e)
{
	lock_guard<mutex> guard(mNetworkEventMutex);
	if (mNetworkEventQueue.size() > 0) {
		e = NetworkEvent(mNetworkEventQueue.front());
		mNetworkEventQueue.pop();
		return true;
	}
	else {
		return false;
	}
}

void Simulator::Run(uvec2 resolution)
{
	mRunning = true;

	Init(resolution);

	sf::Event event;
	sf::Clock timer, fpsTimer;
	NetworkEvent networkEvent;
	float dt = 0.0f;
	int fps = 0;
	while (mRunning) {
		while (mWindow.pollEvent(event)) {
			HandleInput(event);
		}
		while (PollNetworkEvent(networkEvent)) {
			if (mRemoteControl == true)
				HandleNetwork(networkEvent);
		}
		Update(dt);
		Render();

		fps++;
		if (fpsTimer.getElapsedTime().asSeconds() >= 1.0f) {
			mWindow.setTitle("DroneSimulator | Fps: " + to_string(fps));
			fps = 0;
			fpsTimer.restart();
		}

		sf::sleep(sf::seconds(1.0f / 61.0f - timer.getElapsedTime().asSeconds()));

		dt = timer.restart().asSeconds();
	}
}

unique_ptr<Model> Simulator::LoadMarker(string path)
{
	unique_ptr<Model> marker = make_unique<Model>();
	marker->GetMeshes().push_back(MeshBuilder::Plane());
	marker->GetMeshes().at(0)->SetMaterialId(0);
	marker->GetRootNode()->AddMesh(0);
	marker->GetMaterials().push_back(make_unique<Material>());
	marker->GetMaterials().at(0)->AddTexture(Material::eDiffuseMap, make_shared<Texture>(path));
	marker->GetMaterials().at(0)->mColorAmbient = vec3(0.0f, 0.0f, 0.0f);
	marker->GetMaterials().at(0)->mColorDiffuse = vec3(1.0f, 1.0f, 1.0f);
	marker->GetMaterials().at(0)->mColorSpecular = vec3(1.0f, 1.0f, 1.0f);
	marker->GetMaterials().at(0)->mShininess = 16.0f;
	marker->GetMaterials().at(0)->mShininessStrength = 1.0f;
	return move(marker);
}
