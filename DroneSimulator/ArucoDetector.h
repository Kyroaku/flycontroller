#pragma once

#include <opencv2/aruco.hpp>

using namespace cv;

class ArucoDetector {
	Ptr<aruco::Dictionary> mDictionary;

public:
	ArucoDetector(cv::aruco::PREDEFINED_DICTIONARY_NAME dictionary);
};