#pragma once

#include <iostream>
#include <future>

#include <Utility/Network/NetworkManager.h>

#include <ProcessingEngine/Image.h>
#include <Utility/Logger.h>

#include "Simulator.h"

#define CAMERA_WINDOW_NAME		"DroneSimulator"

using namespace std;
using namespace cv;

/// <summary>
/// Drone simulator main class that is entry point of the application.
/// This class is an implementation of low level layer. It mainly implements network communication and starts the </see cref="Simulator">.
/// </summary>
class DroneSimulator {
public:
	/// <summary>
	/// This flag set to true means that application is running. Setting it false will cause that application
	/// will leave the loop.
	/// </summary>
	bool mIsRunning = true;
	/// <summary>
	/// Server socket for video streaming.
	/// </summary>
	Socket mVideoServer;
	/// <summary>
	/// Server socket for drone commands.
	/// </summary>
	Socket mControllerServer;
	/// <summary>
	/// Video stream client.
	/// </summary>
	shared_ptr<Socket> mVideoClient;
	/// <summary>
	/// Drone commands client.
	/// </summary>
	shared_ptr<Socket> mControllerClient;

	/// <summary>
	/// Future object for video streaming thread.
	/// </summary>
	future<void> mVideoSendFuture;
	/// <summary>
	/// Future object for receiving drone commands thread.
	/// </summary>
	future<void> mControlReceiveFuture;

	/// <summary>
	/// Actual simulator object that implements local application behaviour
	/// </summary>
	Simulator mSimulator;

public:
	/// <summary>
	/// Initializes servers for network communication and starts </see cref="Simulator">.
	/// </summary>
	void Run();

private:
	/// <summary>
	/// Initializes network communication and opens the servers.
	/// </summary>
	void InitServer();
	/// <summary>
	/// Waits for video stream connection and accepts it. This is blocking function.
	/// </summary>
	void AcceptVideoClient();
	/// <summary>
	/// Waits for drone controller connection and accepts it. This is blocking function.
	/// </summary>
	void AcceptControllerClient();
};