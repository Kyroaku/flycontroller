#version 330 core

struct Material {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	float mOpacity;
	float mShininess;
	float mShininessStrength;
	float mReflectivity;
};

struct Light {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	vec3 mVector;
	float mConstAttenuation;
	float mLinearAttenuation;
	float mQuadraticAttenuation;
};

out vec4 FragColor;

uniform vec3 uCameraPosition;
uniform mat4 uShadowMatrix;

uniform int uHasDiffuseMap;
uniform int uHasNormalMap;
uniform int uHasSpecularMap;
uniform int uHasDisplacementMap;
uniform int uHasCubeMap;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uSpecularTexture;
uniform sampler2D uDisplacementTexture;
uniform samplerCube uCubeTexture;
uniform sampler2DShadow uShadowTexture;

uniform Material uMaterial;
uniform Light uDirLight;
uniform Light uPointLight;

uniform float uScreenCut;

in GS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vVS;

void main()
{
	/* Parallax Mapping */
	vec3 texCoord = vVS.vTexCoord;
	if(uHasDisplacementMap == 1) {
		vec3 viewDir = normalize(vVS.vTangentRotation * (uCameraPosition - vVS.vPosition));
		vec3 tcStep = vec3(viewDir.xy * 0.1f, 1.0f) / 10.0f;
		float currentDepth = 0.0f;
		while(true) {
			float d = 1.0f - texture2D(uDisplacementTexture, texCoord.xy).r;
			if(d < currentDepth) {
				d = currentDepth - d;
				float d2 = 1.0f - texture2D(uDisplacementTexture, texCoord.xy-tcStep.xy).r - (currentDepth - tcStep.z);
				texCoord.xy = mix(texCoord.xy, texCoord.xy - tcStep.xy, d / (d+d2));
				break;
			}
			texCoord.xy += tcStep.xy;
			currentDepth += tcStep.z;
		}
	}

	/* Diffuse map */
	vec3 diffuseMap = vec3(1.0f);
	if(uHasDiffuseMap == 1)
		diffuseMap = texture2D(uDiffuseTexture, texCoord.xy).rgb;

	/* Normal map */
	vec3 normal = normalize(vVS.vNormal);
	if(uHasNormalMap == 1) {
		normal = texture2D(uNormalTexture, texCoord.xy).xyz * 2.0 - 1.0;
		normal = normalize(vVS.vTangentRotation * normal);
	}

	/* Specular map */
	vec3 specularMap = vec3(1.0f);
	if(uHasSpecularMap == 1)
		specularMap *= texture2D(uSpecularTexture, texCoord.xy).rgb * uMaterial.mShininessStrength;

	/* Lighting */
	vec3 ambientColor = vec3(0.0f);
	vec3 diffuseColor = vec3(0.0f);
	vec3 specularColor = vec3(0.0f);

	float diffuseFactor = 0.0f;
	float shininessFactor = 0.0f;
	vec3 camDir = normalize(uCameraPosition - vVS.vPosition);
	vec3 lightDir = normalize(vVS.vPosition - uPointLight.mVector);

	/* Directional lights */
	//diffuseFactor = max(0.0, dot(-normal, uDirLight.mVector));
	diffuseFactor = abs(dot(-normal, uDirLight.mVector));
	shininessFactor = pow(max(0.0, dot(reflect(uDirLight.mVector, normal), camDir)), uMaterial.mShininess);
	ambientColor += uDirLight.mAmbientColor;
	diffuseColor += uDirLight.mDiffuseColor * diffuseFactor;
	specularColor += uDirLight.mSpecularColor * shininessFactor;
	
	/* Reflections */
	if(uHasCubeMap == 1) {
		vec3 reflectedViewDir = normalize(reflect(-camDir, normal));
		vec3 reflectColor = texture(uCubeTexture, reflectedViewDir).rgb;
		diffuseMap = mix(diffuseMap, reflectColor, uMaterial.mReflectivity);
	}

	/* Final color */
	vec3 color =
		(ambientColor * uMaterial.mAmbientColor +
		diffuseColor * uMaterial.mDiffuseColor) * diffuseMap +
		specularColor * uMaterial.mSpecularColor * specularMap;

	FragColor = vec4(color, uMaterial.mOpacity);
}