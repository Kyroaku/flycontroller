#include "Decoder.h"

#include <iostream>

Decoder::Decoder(AVCodecID decoder)
{
	//av_register_all();

	m_pCodec = avcodec_find_decoder(decoder);
	m_pCodecCtx = avcodec_alloc_context3(m_pCodec);
	avcodec_open2(m_pCodecCtx, m_pCodec, 0);
	m_pFrame = av_frame_alloc();
}

Decoder::~Decoder()
{
	av_free(m_pFrame);
	avcodec_close(m_pCodecCtx);
}

cv::Mat Decoder::decodeStreamData(unsigned char * pData, size_t sz)
{
	AVPacket        packet;
	av_init_packet(&packet);

	packet.data = pData;
	packet.size = (int)sz;
	int ret = avcodec_send_packet(m_pCodecCtx, &packet);
	if (ret) {
		char buff[10000];
		av_strerror(ret, buff, sizeof(buff));
		std::cout << buff << std::endl;
	}
	ret = avcodec_receive_frame(m_pCodecCtx, m_pFrame);
	if (ret) {
		char buff[10000];
		av_strerror(ret, buff, sizeof(buff));
		std::cout << buff << std::endl;
	}

	//AVFrame           *pFrame_BGR24;
	//uint8_t           *buffer_BGR24;
	//// Allocate an AVFrame structure
	//pFrame_BGR24 = av_frame_alloc();
	//if (pFrame_BGR24 == NULL) {
	//	fprintf(stderr, "Could not allocate pFrame_BGR24\n");
	//}
	//// Determine required buffer size and allocate buffer 
	//buffer_BGR24 = (uint8_t *)av_malloc(sizeof(uint8_t)*av_image_get_buffer_size(AV_PIX_FMT_BGR24, m_pCodecCtx->width, m_pCodecCtx->height, 1));
	//// Assign buffer to image planes 
	//av_image_fill_arrays(pFrame_BGR24->data, pFrame_BGR24->linesize, buffer_BGR24, AV_PIX_FMT_BGR24, m_pCodecCtx->width, m_pCodecCtx->height, 1);

	//struct SwsContext *pConvertCtx_BGR24;
	//// format conversion context
	//pConvertCtx_BGR24 = sws_getContext(
	//	m_pCodecCtx->width, m_pCodecCtx->height, m_pCodecCtx->pix_fmt,
	//	m_pCodecCtx->width, m_pCodecCtx->height, AV_PIX_FMT_BGR24,
	//	SWS_SPLINE, NULL, NULL, NULL);

	//sws_scale(pConvertCtx_BGR24, m_pFrame->data, m_pFrame->linesize, 0,
	//	m_pCodecCtx->height, pFrame_BGR24->data, pFrame_BGR24->linesize);

	cv::Mat m;
	AVFrame dst;
	int w = m_pFrame->width;
	int h = m_pFrame->height;
	m = cv::Mat(h, w, CV_8UC3);
	dst.data[0] = (uint8_t *)m.data;
	av_image_fill_arrays(dst.data, dst.linesize, dst.data[0], AV_PIX_FMT_BGR24, w, h, 1);

	AVPixelFormat src_pixfmt = (AVPixelFormat)m_pFrame->format;
	AVPixelFormat dst_pixfmt = AV_PIX_FMT_BGR24;
	SwsContext *convert_ctx = sws_getContext(w, h, src_pixfmt, w, h, dst_pixfmt,
		SWS_FAST_BILINEAR, NULL, NULL, NULL);

	if (convert_ctx == NULL) {
		fprintf(stderr, "Cannot initialize the conversion context!\n");
		exit(1);
	}

	sws_scale(convert_ctx, m_pFrame->data, m_pFrame->linesize, 0, h,
		dst.data, dst.linesize);

	return m;
}
