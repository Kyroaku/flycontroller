#pragma once

extern "C" {
#include <ffmpeg/libavcodec/avcodec.h>
#include <ffmpeg/libavformat/avformat.h>
#include <ffmpeg/libswscale/swscale.h>
#include <ffmpeg/libavutil/frame.h>
#include <ffmpeg/libavutil/imgutils.h>
}
#include <opencv/opencv.hpp>

#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(55,28,1)
#define av_frame_alloc  avcodec_alloc_frame
#endif

#pragma comment(lib, "ffmpeg/lib/avcodec")
#pragma comment(lib, "ffmpeg/lib/avformat")
#pragma comment(lib, "ffmpeg/lib/swscale")
#pragma comment(lib, "ffmpeg/lib/avdevice")
#pragma comment(lib, "ffmpeg/lib/avfilter")
#pragma comment(lib, "ffmpeg/lib/avutil")
#pragma comment(lib, "ffmpeg/lib/swresample")

class Decoder
{
public:
	Decoder(AVCodecID decoder);

	~Decoder();

	cv::Mat decodeStreamData(unsigned char * pData, size_t sz);

protected:
	AVCodecContext  *m_pCodecCtx;
	AVCodec         *m_pCodec;
	AVFrame         *m_pFrame;
};